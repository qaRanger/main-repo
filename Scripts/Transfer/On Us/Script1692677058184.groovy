import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.android.nativekey.AndroidKey
import io.appium.java_client.android.nativekey.KeyEvent
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.MobileTestObject
import com.kms.katalon.core.testobject.MobileTestObject.MobileLocatorStrategy

Mobile.tap(findTestObject('Object Repository/Menu Utama/Icon_Transfer'), 3)

Mobile.tap(findTestObject('Object Repository/Menu Transfer/Clickable_Image_Transfer'), 3 )

Mobile.tap(findTestObject('Object Repository/Menu Transfer/Transfer/Field_AccountNumber'), 3)

Mobile.sendKeys(findTestObject('Object Repository/Menu Transfer/Transfer/Field_AccountNumber'), AcctNum)
Mobile.hideKeyboard()
Mobile.delay(10)
Mobile.tap(findTestObject('Object Repository/Menu Transfer/Transfer/Field_InputAmount'), 1)

Mobile.sendKeys(findTestObject('Object Repository/Menu Transfer/Transfer/Field_InputAmount'), )
Mobile.hideKeyboard()

Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Menu Transfer/Transfer/Field_SelectSourceAccount1'), 2)s

device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.30
int endY = device_Height * 0.70
Mobile.swipe(startX, endY, endX, startY)

